//
//  CategoryData.swift
//  Indian Cuisine Food Corte
//
//  Created by Admin on 2018-07-18.
//  Copyright © 2018 Depanneur. All rights reserved.
//

import UIKit

class CategoryData: NSObject {

    var categoryId: Int?
    var categoryName: String?
    
    init(_ categoryId: Int, _ categoryName: String) {
        self.categoryId = categoryId
        self.categoryName = categoryName
    }
    
    class func getAllCategories() -> [CategoryData] {
        var categoryDataList = [CategoryData]()
        
        var categoryData = CategoryData(1, "Snacks/Appetizers")
        categoryDataList.append(categoryData)
        
        categoryData = CategoryData(2, "RIZ/RICE")
        categoryDataList.append(categoryData)
       
        categoryData = CategoryData(3, "AGNEAU/LAMB");
        categoryDataList.append(categoryData)
        
        categoryData = CategoryData(4, "FRUITS DE MER/SEAFOOD");
        categoryDataList.append(categoryData)
        
        categoryData = CategoryData(5, "POULET/CHICKEN");
        categoryDataList.append(categoryData)
        
        categoryData = CategoryData(6, "POUR DEUX PERSONNES/COMBO SPECIAL");
        categoryDataList.append(categoryData)
        
        categoryData = CategoryData(7, "POUR I PERSONNE/SPECIAL THALI");
        categoryDataList.append(categoryData)
        
        categoryData = CategoryData(8, "PAINS INDIENS/INDIAN BREADS");
        categoryDataList.append(categoryData)
        
        categoryData = CategoryData(9, "DESSERTS");
        categoryDataList.append(categoryData)
        
        categoryData = CategoryData(10, "BOISSOINS/DRINKS");
        categoryDataList.append(categoryData)
        
        categoryData = CategoryData(11, "POUR I PERSONNE/VEGETARIAN SPECIAL THALI");
        categoryDataList.append(categoryData)
        
        return categoryDataList
    }
}
