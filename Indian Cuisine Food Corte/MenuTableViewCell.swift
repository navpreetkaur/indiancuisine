//
//  MenuTableViewCell.swift
//  Indian Cuisine Food Corte
//
//  Created by Admin on 2018-07-18.
//  Copyright © 2018 Depanneur. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var snackPrice: UILabel!
    @IBOutlet weak var snackName: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
