//
//  SnacksData.swift
//  Indian Cuisine Food Corte
//
//  Created by Admin on 2018-07-18.
//  Copyright © 2018 Depanneur. All rights reserved.
//

import UIKit

class SnacksData: NSObject {

    var snackId: Int?
    var snackName: String?
    var snackPrice: Double?
    var categoryId: Int?
    var quantity: Int?
    
    init(_ snackId: Int, _ snackName: String, _ snackPrice: Double, _ categoryId: Int) {
        self.snackId = snackId
        self.snackName = snackName
        self.snackPrice = snackPrice
        self.categoryId = categoryId
        self.quantity = 0
    }
    
    override init() {
        self.snackId = 0
        self.snackName = ""
        self.snackPrice = 0
        self.categoryId = 0
        self.quantity = 0
    }
    private func getAllSnacks() -> [SnacksData] {
        
        var snacksList = [SnacksData]()
        
        var snacksData = SnacksData(1, "Vegi Samosa", 2.99, 1)
        snacksList.append(snacksData)
        
        snacksData = SnacksData(2, "Paneer Tikka", 9.99, 1);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(3, "Vegetable Pakora", 3.49, 1);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(4, "Chicken Samosa", 3.99, 1);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(5, "Bread/Pakora", 2.99, 1);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(6, "Riz cuit a la vapeur/Steam Rice", 1.99, 2);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(7, "Biryani de legumes/Vegetable Biryani", 6.99, 2);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(8, "Poulet Biryani/Chicken Briyani", 8.99, 2);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(9, "Beaf Briyani", 8.99, 2);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(10, "Cari d' Agneau/ Lamb Curry", 9.99, 3);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(11, "Agneau/Lamb Kadhai", 9.99, 3);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(12, "Agneau/Lamb Kabab", 7.99, 3);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(13, "Agneau/Lamb Tikka", 9.99, 3);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(14, "Saumon Masala/ Fish Masala", 12.99, 4);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(15, "Saumon Tikka/ Fish Tikka", 12.99, 4);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(16, "Curry De Poulet(sans os)/Chicken Curry(Boneless)", 9.99, 4);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(17, "Poulet Au Beurre(sans os)/Butter Chicken(Boneless)", 10.99, 5);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(18, "Poulet/Chicken Kadhai", 9.99, 5);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(19, "Poulet Masala avec Piment/ Chili Chicken", 10.99, 5);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(20, "Poulet Tikka Masala/Chicken Tikka Masala", 10.99, 5);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(21, "Poulet Palak/Palak Chicken", 9.99, 5);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(22, "Poulet/Chicken Kabab 2pcs.", 6.99, 5);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(23, "Poulet jambe 2pcs./Chicken Leg 2pcs.", 5.99, 5);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(24, "Curry Au Poulet avec du riz, yogourt, 2 chapaati ou naan et salade/Chicken Curry", 15.99, 6);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(25, "Kadhai poulet avec du riz, yogourt, 2 chapaati ou naan et salade/Kadhai Chicken", 15.99, 6);
        snacksList.append(snacksData)
        
        snacksList.append(snacksData)
        snacksData = SnacksData(26, "Poulet Au beurre avec du riz, yogourt, 2 chapaati ou naan et salade/Butter Chicken", 15.99, 6);
        
        snacksData = SnacksData(27, "Curry au porc avec du riz, yogourt, 2 chapaati ou naan et salade/pork Curry", 15.99, 6);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(28, "Kadhai porc avec du riz, yogourt, 2 chapaati ou naan et salade/pork Kadhai", 15.99, 6);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(29, "Curry d'agneau avec du riz, yogourt, 2 chapaati ou naan et salade/Lamb Curry", 15.99, 6);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(30, "Kadhai d'agneau avec du riz, yogourt, 2 chapaati ou naan et salade/Lamb Kadhai", 15.99, 6);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(31, "Curry du boeuf avec du riz, yogourt, 2 chapaati ou naan et salade/Beif Curry", 15.99, 6);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(32, "Kadhai boeuf avec du riz, yogourt, 2 chapaati ou naan et salade/Beif Kadhai", 15.99, 6);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(33, "Curry au poulet ou vegi 1 naan, riz, yogourt, salade et 1 boission gazeuse/ Chicken Curry", 8.99, 7);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(34, "poulet au beurre ou vegi 1 naan, riz, yogourt, salade et 1 boission gazeuse/Butter Chicken", 8.99, 7);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(35, "Curry au porc ou vegi 1 naan, riz, yogourt, salade et 1 boission gazeuse/ Pork Curry", 8.99, 7);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(36, "Curry d'agneau ou vegi 1 naan, riz, yogourt, salade et 1 boission gazeuse/Lamb Curry", 8.99, 7);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(37, "Plain Naan", 1.59, 8);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(38, "Ail Naan/Garlic Naan", 1.79, 8);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(39, "Chappati", 1.25, 8);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(40, "Rasgulla 1pc.", 1.49, 9);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(41, "Gulab Jamun 1pc.", 1.49, 9);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(42, "Mango Lassi", 2.99, 10);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(43, "Mango Secouer/Mango Shake", 2.99, 10);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(44, "Sweet & Salt Lassi", 1.99, 10);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(45, "Jus Mango/Mango Juice", 1.99, 10);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(46, "Jus Orange/Orange Juice", 1.99, 10);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(47, "Indian Tea", 1.79, 10);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(48, "Plat Mitonne Du Riz, Yogourt, 2 Chapaati Ou Naan Et Salade/mutter panner", 8.99, 11);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(49, "Epinards Paneer Du Riz, Yogourt, 2 Chapaati Ou Naan Et Salade/plak paneer", 8.99, 11);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(50, "Daal Makhni Du Riz, Yogourt, 2 Chapaati Ou Naan Et Salade/dal Makhni", 8.99, 11);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(51, "1 Pizza 12\" - \n +10 alies de poulet \n +ou 2 petites poutines \n +ou 2 petites salade cesar \n ou 1 moyenne lasagne", 20.99, 12);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(52, "1 Pizza 14\" - \n +15 alies de poulet \n +ou 4 petites poutines \n +ou 4 petites salade cesar", 25.99, 12);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(53, "1 Pizza 16\" - \n +10 alies de poulet \n +Frites \n +4 Pepsi 355 ml.", 29.99, 12);
        snacksList.append(snacksData)
        
        snacksData = SnacksData(54, "2 Pizza 12\" - \n +10 alies de poulet \n +Frites moyenne \n +4 Pepsi 355 ml.", 28.99, 12);
        snacksList.append(snacksData)
        
        return snacksList
    }
    
    func getSnacksAccordingToCategory(categoryId: Int) -> [SnacksData]{
        let allSnacks = getAllSnacks()
        let filteredSnacks = allSnacks.filter { categoryId == $0.categoryId }
        return filteredSnacks
    }
}
