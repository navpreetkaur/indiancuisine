//
//  ViewController.swift
//  Indian Cuisine Food Corte
//
//  Created by Admin on 2018-07-18.
//  Copyright © 2018 Depanneur. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var navigationBar: UINavigationBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    var categoryList = [CategoryData]()
    var snacksList = [SnacksData]()
    
    let snackData = SnacksData()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        categoryList = CategoryData.getAllCategories()
        return categoryList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        snacksList = snackData.getSnacksAccordingToCategory(categoryId: self.categoryList[section].categoryId!)
        return snacksList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let list: [SnacksData] = self.snackData.getSnacksAccordingToCategory(categoryId: self.categoryList[indexPath.section].categoryId!)
        let snackData = list[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MenuTableViewCell
        if cell != nil {
            cell?.snackName.text = snackData.snackName!
            cell?.snackPrice.text = "\(snackData.snackPrice!)"
            
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 40))
        
        view.backgroundColor = .red
        let label = UILabel(frame: CGRect(x: 5, y: 0, width: self.tableView.frame.width, height: 40))
        label.text = categoryList[section].categoryName!
        label.textColor = .white
        
        view.addSubview(label)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}
